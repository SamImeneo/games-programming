// GridWorld.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "GridWorld.h"

#include <iostream>
#include <stdio.h>
#include <string>
#include <thread>

using namespace std;

bool gameOver = false;

int playerX = 7;
int playerY = 2;

char userInput;

bool north = false;
bool south = false;
bool east = false;
bool west = false;

time_t start = time(0);
double seconds_since_start;
double printedTime;


/*
########
#G D#D #
#   #  #
### # D#
#   #  #
# #### #
#      #
##S#####

1 = #
2 = D
3 = G
4 = " " (blank space)
5 = S

11111111
13421241
14441441
11141421
14441441
14111141
14444441
11511111
*/
// 2D array to hold game board here

int boardArray[8][8] =
{
	{ 1,1,1,1,1,1,1,1 },
	{ 1,3,4,2,1,2,4,1 },
	{ 1,4,4,4,1,4,4,1 },
	{ 1,1,1,4,1,4,2,1 },
	{ 1,4,4,4,1,4,4,1 },
	{ 1,4,1,1,1,1,4,1 },
	{ 1,4,4,4,4,4,4,1 },
	{ 1,1,5,1,1,1,1,1 }
};

int main()
{
	//cout << "you are now at: " << playerY << ", " << playerX << endl;
	//cout << "type: " << boardArray[playerX][playerY] << endl;


	thread userInputThread(getUserInput);
	
	while (gameOver == false)
	{
		Update();
	}
	userInputThread.join();
    return 0;
}

void getUserInput()
{
	// while the game is running
	while (gameOver == false)
	{
		// if a user input hasnt been made
		if (!userInput)
		{
			// check which way they want to go
			cout << "Which direction would you like to move?" << endl;
			// check their availible moves
			checkMoves();

			// basic output
			cout << "You can move: " << endl;

			// if they can move north
			if (north)
			{
				// output north
				cout << "North (n)" << endl;
			}
			if (south)
			{
				// output south
				cout << "South (s)" << endl;
			}
			if (west)
			{
				// output west
				cout << "West (w)" << endl;
			}
			if (east)
			{
				// output east
				cout << "East (e)" << endl;
			}
			// reset which moves are availible (so they arent falsly given as options on next loop)
			resetMoves();
			// put their input into userInput variable
			cin >> userInput;
		}

		if (boardArray[playerX][playerY] == 2)
		{
			// player loses
			cout << "You Lose!" << endl << "Enter 'Q' to quit" << endl;
			cin >> userInput;
			gameOver = true;
		}

		if (boardArray[playerX][playerY] == 3)
		{
			// player wins
			cout << "You found the gold! You Win!" << endl << "Enter 'Q' to quit" << endl;
			cin >> userInput;
			gameOver = true;
		}

	}
	return;
}

void Update()
{
	if (userInput)
	{
		// check that user input was a valid command
		if (toupper(userInput) == 'Q' || toupper(userInput) == 'N' || toupper(userInput) == 'S' || toupper(userInput) == 'E' || toupper(userInput) == 'W')
		{
			//if it was, try and do the command here
			//cout << "You're input was: " << userInput << endl;

			// if user enters Q
			if (toupper(userInput) == 'Q')
			{
				// end the game
				gameOver = true;
			}

			// if user enters N
			if (toupper(userInput) == 'N')
			{
				// if position north of player is free
				if (boardArray[playerX - 1][playerY] == 1)
				{
					// player is trying to walk into a wall
					cout << "Cannot move that way, please enter a valid direction" << endl;
				}
				else
				{
					// check the move wont move the player out of bounds
					if ((playerX - 1) >= 0)
					{
						//cout << "you were at: " << playerY << ", " << playerX << endl;
						//cout << "type: " << boardArray[playerX][playerY] << endl;
						playerX--;
						//cout << "you are now at: " << playerY << ", " << playerX << endl;
						//cout << "type: " << boardArray[playerX][playerY] << endl;
					}
					else
					{
						// if input was invalid, tell them here
						cout << "Cannot move that way, out of bounds" << endl;
					}
				}
			}

			// if user enters S
			if (toupper(userInput) == 'S')
			{
				// if position south of player is free
				if (boardArray[playerX + 1][playerY] == 1)
				{
					// player is trying to walk into a wall
					cout << endl << "Cannot move that way, please enter a valid direction" << endl;
				}
				else
				{
					// check the move wont move the player out of bounds
					if ((playerX + 1) < 8)
					{
						//cout << "you were at: " << playerY << ", " << playerX << endl;
						//cout << "type: " << boardArray[playerX][playerY] << endl;
						playerX++;
						//cout << "you are now at: " << playerY << ", " << playerX << endl;
						//cout << "type: " << boardArray[playerX][playerY] << endl;
					}
					else
					{
						// if input was invalid, tell them here
						cout << "Cannot move that way, out of bounds" << endl;
					}
				}
			}

			// if user enters W
			if (toupper(userInput) == 'W')
			{
				// if position west of player is free
				if (boardArray[playerX][playerY - 1] == 1)
				{
					// player is trying to walk into a wall
					cout << "Cannot move that way, please enter a valid direction" << endl;
				}
				else
				{
					// check the move wont move the player out of bounds
					if ((playerY - 1) >= 0)
					{
						//cout << "you were at: " << playerY << ", " << playerX << endl;
						//cout << "type: " << boardArray[playerX][playerY] << endl;
						playerY--;
						//cout << "you are now at: " << playerY << ", " << playerX << endl;
						//cout << "type: " << boardArray[playerX][playerY] << endl;
					}
					else
					{
						// if input was invalid, tell them here
						cout << "Cannot move that way, out of bounds" << endl;
					}
				}
			}

			// if user enters E
			if (toupper(userInput) == 'E')
			{
				// if position east of player is free
				if (boardArray[playerX][playerY + 1] == 1)
				{
					// player is trying to walk into a wall
					cout << "Cannot move that way, please enter a valid direction" << endl;
				}
				else
				{
					// check the move wont move the player out of bounds
					if ((playerY + 1) < 8)
					{
						//cout << "you were at: " << playerY << ", " << playerX << endl;
						//cout << "type: " << boardArray[playerX][playerY] << endl;
						playerY++;
						//cout << "you are now at: " << playerY << ", " << playerX << endl;
						//cout << "type: " << boardArray[playerX][playerY] << endl;
					}
					else
					{
						// if input was invalid, tell them here
						cout << "Cannot move that way, out of bounds" << endl;
					}
				}
			}
		}
		else
		{
			// if input was invalid, tell them here
			cout << "Please enter a valid direction" << endl;
		}
		userInput = NULL;
	}


	// get the amount of seconds since the program started
	seconds_since_start = difftime(time(0), start);
	
	// if the amount of time in seconds hasnt been printed
	if (printedTime != seconds_since_start)
	{
		// set that time in seconds to the desired output
		printedTime = seconds_since_start;
		// output it
		cout << printedTime << endl;
	}
}

// check which directions the player can move in
void checkMoves()
{
	// if the player can move north, set it to true.
	if (boardArray[playerX - 1][playerY] != 1)
	{
		north = true;
	}
	// if the player can move south, set it to true.
	if (boardArray[playerX + 1][playerY] != 1 && playerX != 7)
	{
		south = true;
	}
	// if the player can move west, set it to true.
	if (boardArray[playerX][playerY - 1] != 1)
	{
		west = true;
	}
	// if the player can move east, set it to true.
	if (boardArray[playerX][playerY + 1] != 1)
	{
		east = true;
	}
}

void resetMoves()
{
	// reset all direction bools to false, this is called after each player input
	// this prevents giving the player false information as to which way they can move.
	north = false;
	south = false;
	west = false;
	east = false;
}