#pragma once
#include "Inventory.h"
#include "Location.h"


class Player
{
public:
	Player();
	~Player();
	void draw();
	void addItem(Item);
	void removeItem(int);
	int getInventorySize();
	Item getItem(int);

	void setLocation(Location);
	Location getLocation();

private:
	Inventory lInventory;
	Location lLocation;
};

