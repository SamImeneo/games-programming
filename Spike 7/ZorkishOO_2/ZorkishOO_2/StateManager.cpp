#include "StateManager.h"

using namespace std;
StateManager::StateManager()
{
	currentState = &lMainMenu;
}


StateManager::~StateManager()
{
}

void StateManager::draw()
{
	currentState->draw();
}

void StateManager::handleInput()
{
	currentState->handleInput(this);
}

void StateManager::switchState(string userInput)
{
	if (userInput == "1" || userInput == "Q")
	{
		currentState = &lMainMenu;
	}else if (userInput == "2")
	{
		currentState = &lAbout;
	}
	else if (userInput == "3")
	{
		currentState = &lHelp;
	}
	else if (userInput == "4")
	{
		currentState = &lSelectAdventure;
	}
	else if (userInput == "5")
	{
		currentState = &lHallOFame;
	}
	else if (userInput == "6")
	{
		currentState = &lHighscore;
	}
	else if (userInput == "7")
	{
		currentState = &lGameplay;
	}
	else if (userInput == "8")
	{
		currentState = &lAdventure;
	}
	

}