#pragma once
#include "Item.h"
#include <vector>

class Inventory
{
public:
	Inventory();
	~Inventory();
	void addItem(Item);
	void removeItem(int);
	int getInventorySize();
	Item getItem(int i);
private:
	std::vector<Item> lInventory;

};

