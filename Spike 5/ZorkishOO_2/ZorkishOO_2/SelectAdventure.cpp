#include "SelectAdventure.h"
#include <iostream>
#include <string>
#include "StateManager.h"

using namespace std;
SelectAdventure::SelectAdventure()
{
}


SelectAdventure::~SelectAdventure()
{
}

void SelectAdventure::draw()
{
	cout << "Select Adventure:" << endl;
	cout << "To play the Test Level, enter <TEST>" << endl;
	cout << endl << "To return to the Main Menu, enter <1>" << endl;
}

void SelectAdventure::handleInput(StateManager *lStateManager)
{
	string userInput;
	cin >> userInput;
	if (userInput == "1")
	{
		lStateManager->switchState(userInput);
	}
	if (userInput == "TEST")
	{
		lStateManager->switchState(userInput);
	}
}
