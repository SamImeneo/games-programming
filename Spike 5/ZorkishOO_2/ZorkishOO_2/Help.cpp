#include "Help.h"
#include <iostream>
#include <string>
#include "StateManager.h"

using namespace std;
Help::Help()
{
}


Help::~Help()
{
}

void Help::draw()
{
	cout << "Help:" << endl;
	cout << "To navigate to a menu, enter the key show in the square braces<> at the end of the line" << endl;
	cout << endl << "To return to the Main Menu, enter <1>" << endl;
}

void Help::handleInput(StateManager *lStateManager)
{
	string userInput;
	cin >> userInput;

	if (userInput == "1")
	{
		lStateManager->switchState(userInput);
	}
}
