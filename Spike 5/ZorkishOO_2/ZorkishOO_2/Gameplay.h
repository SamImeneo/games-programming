#pragma once
#include "State.h"
class Gameplay :
	public State
{
public:
	Gameplay();
	~Gameplay();
	void draw();
	void handleInput(StateManager*);
};

