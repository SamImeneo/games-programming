#pragma once
#include "Item.h"
#include "Inventory.h"

class Bag :
	public Item
{
public:
	Bag();
	~Bag();

	Inventory* getInventory();
	int getInventorySize();

private:
	Inventory lInventory;
};

