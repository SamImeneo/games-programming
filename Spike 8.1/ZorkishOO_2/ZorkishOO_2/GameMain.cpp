#include "GameMain.h"

#include <stdio.h>
#include <iostream>

using namespace std;

GameMain::GameMain()
{
	gameRunning = true;
	lStateManager.draw();

	gameLoop();

	system("pause");
}


GameMain::~GameMain()
{
}


void GameMain::gameLoop()
{
	while (gameRunning)
	{
		lStateManager.handleInput();
		lStateManager.draw();
	}
}
