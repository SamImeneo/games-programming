#pragma once
#include "StateManager.h"

#include <string>

using namespace std;

class GameMain
{
	//fields here
	string gameState;

public:
	GameMain();
	~GameMain();
	void gameLoop();

	bool gameRunning;


private:
	StateManager lStateManager;
	
};

