#include "CommandDoer.h"


using namespace std;
CommandDoer::CommandDoer()
{
}


CommandDoer::~CommandDoer()
{
}

void CommandDoer::checkForLocation(string loc, Player& lPlayer, vector<Location>& locationVector, vector<Path>& pathVector)
{
	// loc = where the player wants to go
	system("CLS");
	// if the player wants to go to their current location
	if (loc == lPlayer.getLocation().getName())
	{
		// tell them they are already there
		cout << "You are already at the " << lPlayer.getLocation().getName() << endl;
	}
	else // otherwise
	{
		// for each location that exists
		for each (Location l in locationVector)
		{
			// check if the location exists
			if (loc == l.getName())
			{
				// if it does, for each path
				for each (Path p in pathVector)
				{
					// check if this path joins those two locations

					// points holds 2 strings, each on the name of a location at either end of that path
					vector<Location> points;
					points.push_back(p.getPoints()[0]);
					points.push_back(p.getPoints()[1]);

					// first point = the users location, and the second point = thei destination, there is a valid path
					if ((points[0].getName() == lPlayer.getLocation().getName()) && (points[1].getName() == loc))
					{
						system("CLS");
						for (int i = 0; i < locationVector.size(); i++)
						{
							if (locationVector[i].getName() == points[1].getName())
							{
								lPlayer.setLocation(locationVector[i]);
								system("CLS");
								break;
							}
						}
						break;
					}
					else if ((points[0].getName() == loc) && (points[1].getName() == lPlayer.getLocation().getName()))
					{
						system("CLS");
						for (int i = 0; i < locationVector.size(); i++)
						{
							if (locationVector[i].getName() == points[0].getName())
							{
								lPlayer.setLocation(locationVector[i]);
								system("CLS");
								break;
							}
						}
						break;
					}
					cout << "you cannot go that way" << endl;
				}
			}
		}
	}
}

void CommandDoer::lookItem(string item, Player& lPlayer)
{
	Location loc = lPlayer.getLocation();
	Inventory locInv = loc.getInventory();
	bool descFound = false;
	
	for each (Item i in locInv.getInventory())
	{
		if (item == i.getItemName())
		{
			cout << i.getDescription() << endl;
			descFound = true;
		}
	}
	if (!descFound)
	{
		cout << "You cannot see " << item << endl;
	}
}

void CommandDoer::lookAtIn(string item, string location, Player& lPlayer, vector<Location>& locationVector, vector<Path>& pathVector)
{
	// check if the players location, is the location they are looking in
	if (lPlayer.getLocation().getName() == location)
	{
		Inventory locInv = lPlayer.getLocation().getInventory();
		
		for (int i = 0; i < locInv.getInventorySize(); i++)
		{
			if (locInv.getItem(i).getItemName() == item)
			{
				cout << locInv.getItem(i).getDescription() << endl;
			}
		}
	}// check if the location is the players bag
	else if (location == lPlayer.getBag().getItemName())
	{
		lookForItemInBag(item, lPlayer);
	}
	else
	{
		cout << "There is no " << item << " in " << location << endl;
	}
}

void CommandDoer::lookForItemInBag(string item, Player& lPlayer)
{
	for (int j = 0; j < lPlayer.getBag().getInventory()->getInventorySize(); j++)
	{
		if (item == lPlayer.getBag().getInventory()->getItem(j).getItemName())
		{
			cout << lPlayer.getBag().getInventory()->getItem(j).getDescription() << endl;
		}
	}
}


