#pragma once
#include "Inventory.h"


class Player
{
public:
	Player();
	~Player();
	void draw();
	void addItem(Item);
	void removeItem(int);
	int getInventorySize();
	Item getItem(int);
private:
	Inventory lInventory;
};

