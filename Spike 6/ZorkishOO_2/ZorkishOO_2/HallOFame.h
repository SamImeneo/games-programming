#pragma once
#include "State.h"
class HallOFame :
	public State
{
public:
	HallOFame();
	~HallOFame();
	void draw();
	void handleInput(StateManager*);
};

