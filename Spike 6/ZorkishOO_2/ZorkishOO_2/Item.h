#pragma once
#include <iostream>

class Item
{
public:
	Item();
	~Item();
	void setName(std::string);
	std::string getItemName();

private:
	std::string lName;
};

