#include "SelectAdventure.h"
#include <iostream>
#include <string>
#include "StateManager.h"

using namespace std;
SelectAdventure::SelectAdventure()
{
}


SelectAdventure::~SelectAdventure()
{
}

void SelectAdventure::draw()
{
	system("CLS");
	cout << "Select Adventure:" << endl;
	cout << "To play the Test Level, enter <7>" << endl;
	cout << endl << "To return to the Main Menu, enter <1>" << endl;
}

void SelectAdventure::handleInput(StateManager *lStateManager)
{
	string userInput;
	cin >> userInput;
	if (userInput == "1")
	{
		lStateManager->switchState(userInput);
	}
	if (userInput == "7")
	{
		lStateManager->switchState(userInput);
	}
}
